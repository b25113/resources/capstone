from abc import ABC, abstractclassmethod

class Person(ABC):
	@abstractclassmethod
	def getFullName(self,firstName,lastName):
		pass

	def addRequest(self):
		pass

	def checkRequest(self):
		pass

	def addUser(self,firstName,lastName):
		pass


class Employee(Person):
	def __init__(self,firstName,lastName,email,depart):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._depart = depart

	def get_firstName(self):
		return self._firstName

	def get_lastName(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_depart(self):
		return self._depart

	def set_firstName(self):
		self._firstName = firstName

	def set_lastName(self):
		self._lastName = lastName

	def set_email(self):
		self._email = _email

	def set_depart(self):
		self._depart = depart

	
	def getFullName(self):
		print(f"{self._firstName} {self._lastName}")
	def addRequest(self):
		print("Request has been added")

	def checkRequest(self):
		print(f"{self._request}")

	def addUser(self):
		print(f"{self._firstName} {self._lastName}")

	def login(self):
		print(f"{self._email} has logged in")

	def logout(self):
		print(f"{self._email} has logged out")

class TeamLead(Person):
	def __init__(self,firstName,lastName,email,depart):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._depart = depart

	def get_firstName(self):
		return self._firstName

	def get_lastName(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_depart(self):
		return self._depart

	def set_firstName(self):
		self._firstName = firstName

	def set_lastName(self):
		self._lastName = lastName

	def set_email(self):
		self._email = _email

	def set_depart(self):
		self._depart = depart

	def get_members(self):
		return self._firstName
		return self._lastName

	
	def getFullName(self):
		print(f"{self._firstName} {self._lastName}")
	def addRequest(self):
		print("Request has been added")

	def checkRequest(self):
		print(f"{self._request}")

	def addUser(self):
		print(f"{self._firstName} {self._lastName}")

	def login(self):
		print(f"{self._email} has logged in")

	def logout(self):
		print(f"{self._email} has logged out")

	def addMember(self):
		print(f"{self._firstName} {self._lastName}")

class Admin(Person):
	def __init__(self,firstName,lastName,email,depart):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._depart = depart

	def get_firstName(self):
		return self._firstName

	def get_lastName(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_depart(self):
		return self._depart

	def set_firstName(self):
		self._firstName = firstName

	def set_lastName(self):
		self._lastName = lastName

	def set_email(self):
		self._email = _email

	def set_depart(self):
		self._depart = depart

	
	def getFullName(self):
		print(f"{self._firstName} {self._lastName}")
	def addRequest(self):
		print("Request has been added")

	def checkRequest(self):
		print(f"{self._request}")

	def addUser(self):
		print(f"{self._firstName} {self._lastName}")

	def login(self):
		print(f"{self._email} has logged in")

	def logout(self):
		print(f"{self._email} has logged out")

	def addUser():
		print("New user added")


class Request():
	def __init__(self,name,requester,dateRequested):
		self._name = name
		self._requester = requester
		self._dateRequested = dateRequested
		self._status = "close"

	def updateRequest(self,name):
		pass

	def closeRequest(self):
		print(f"{self._name} has been {self._status}")

	def cancelRequest(self):
		pass

	def get_status(self):
		return self.get_status

	def set_status(self):
		self._status = status




employee1 = Employee("John","Doe","djohn@mail.com","Marketing")
employee2 = Employee("Jane","Smith","sjane@mail.com","Marketing")
employee3 = Employee("Robert","Patterson","probert@mail.com","Sales")
employee4 = Employee("Brandon","Smith","sbrandon@mail.com","Sales")
admin1 = Admin("Monika","Justin","jmonika@mail.com","Marketing")
teamlead1 = TeamLead("Michael","Specter","smicahel@mail.com","Sales")
req1 = Request("New hire orientation",teamlead1,"27-Jul-2021")
req2 = Request("Laptop Repair",employee1,"1-Jul-2021")

# employee1.getFullName()
# admin1.getFullName()
# employee2.login()
# employee2.addRequest()
# employee2.logout()
# assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
# assert admin1.getFullName() == "Monika Justin","Full name should be Monika Justin"
# assert employee2.login() == "sjane@mail.com has logged in"
# assert employee2.addRequest() == "Request has been added"
# assert employee2.logout() == "sjane@mail.com has logged out"

teamlead1.addMember(employee3)
teamlead1.addMember(employee4)

for indiv.emp in teamlead1.get_members():
	print(indiv.emp.getFullName())

#assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())







